# Set the path to the directory where the .kml files are located
$kmlPath = "path_to_kml_files"

# Get all the .kml files in the directory
$kmlFiles = Get-ChildItem $kmlPath -Filter *.kml

# Initialize a counter for the number of files processed
$fileCount = 0

# Loop through the .kml files
foreach ($file in $kmlFiles) {        
    $commandToRun = "ogr2ogr -f CSV ""{0}.csv"" ""{1}"" -lco GEOMETRY=AS_WKT" -f $fileCount.ToString(), $file.FullName
    Write-Output $commandToRun
    Invoke-Expression $commandToRun
    $fileCount++
}

Write-Output "Converted $fileCount .kml files to CSVs"
