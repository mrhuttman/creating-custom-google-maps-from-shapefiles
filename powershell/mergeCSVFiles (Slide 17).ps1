# Set the path to the directory where the .kml files are located
$filePath = "csv_file_path"

# Get all the .csv files in the directory
$selectedFiles = Get-ChildItem $filePath -Filter *.csv

# Set the path for the merged file
$mergedFilePath = "merged_file_path"

# Initialize a counter for the number of files processed
$fileCount = 0

# Loop through the files
foreach ($file in $selectedFiles) {
    if ($fileCount -eq 0)
    {
        $content = Get-Content $file.FullName | Select-Object    
    }
    else 
    {
        # Skip header on all files past the first one
        $content = Get-Content $file.FullName | Select-Object -Skip 1
    }
    Write-Output "Appending" + $file.FullName
    Add-Content -Path $mergedFilePath -Value $content      
    $fileCount++
}

Write-Output "Merged $fileCount files into $mergedFilePath"
