SELECT 
'{ "type": "FeatureCollection", "features":' +

(SELECT
'Feature' as [type],
-- dbo.geometryToJSON is a custom function
JSON_QUERY(VDMS_NC_GIS.dbo.geometryToJSON( (geometry::STGeomFromText(ap.WKT, 0)) )) as [geometry]
, ap.county_nam as 'properties.county'
, ap.prec_id as [properties.id]
, (geometry::STGeomFromText(ap.WKT, 0)).STGeometryType() as 'properties.sqlgeotype'
, '' as 'properties.wkt'
-- Every additional field needs to be mapped to an alias in order to work with FOR JSON PATH
, 'Example' as 'properties.example'
FROM [YOUR_DATABASE].[dbo].[AllPrecincts] ap
	where ap.county_nam = 'CABARRUS'
	and ap.prec_id in ('01-02')
	order by ap.prec_id  
FOR JSON PATH)

+ '}' as OutJSON