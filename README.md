# creating-custom-google-maps-from-shapefiles

This repository contains code examples to accompany the "Renegade Squirrel - Creating custom Google Maps from shapefiles" presentation. Inside this repository you will find scripts that can help you convert your shapefiles into data you can add to your custom Google Maps.

## Shameless plug
Do you know of a Progressive downballot campaign in North Carolina that needs extra help? Enter Renegade Squirrel! We are committed to helping Progressive campaigns win more elections. We want to help the _renegades_ shake up the political ecosystem from School Board to Senate. Visit [Renegade Squirrel](https://renegade-squirrel.com) for more info.

## URLs

[Presentation](https://docs.google.com/presentation/d/111g7skkLW_vCfYFP35naGe5NUnQjS5pWRiC62cb_y_Y/)

[ogr2ogr Homepage](https://gdal.org/programs/ogr2ogr.html)

[Get Google Maps API Key](https://developers.google.com/maps/documentation/javascript/get-api-key)

[NC Board Of Elections FTP Site](https://dl.ncsbe.gov/)

[SQL Server: STGeomFromText docs](https://learn.microsoft.com/en-us/sql/t-sql/spatial-geometry/stgeomfromtext-geometry-data-type?view=sql-server-ver16)

## Tech stack
MS SQL Server 2016 or later ([Download SQL Server 2022 Express](https://www.microsoft.com/en-us/download/details.aspx?id=104781))

Powershell

Your favorite code editor (I like [VS Code](https://code.visualstudio.com/))

A good plain-text editor (I recommend [Notepad++](https://notepad-plus-plus.org/) or [Sublime Text](https://www.sublimetext.com/))

## Misc.
You can use ogr2ogr to import a shapefile directly into a SQL Server database. I couldn't get that feature to play nicely with my local instance of SQL Server, so I opted to convert into CSV and then import the data via SSMS instead. Any DB you use will be able to process CSV data.

If you have trouble viewing all of the GeoJSON returned from a SQL query, see [this](https://stackoverflow.com/questions/11897950/how-do-you-view-all-text-from-an-ntext-or-nvarcharmax-in-ssms)

## Credits
Written by Mike Huttman for Renegade Squirrel, Inc.

## Contact
[My Portfolio](https://mikehuttman.com)

[Renegade Squirrel](https://renegade-squirrel.com)

[Email](mailto:mike@renegade-squirrel.com)